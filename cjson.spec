Name:           cjson
Version:        1.7.18
Release:        3
Summary:        Ultralightweight JSON parser in ANSI C
 
License:        MIT
URL:            https://github.com/DaveGamble/cJSON
Source0:        https://github.com/DaveGamble/cJSON/archive/refs/tags/v%{version}.tar.gz

Patch1001:	backport-check-for-null-in-cJSON_DetachItemViaPointer.patch

BuildRequires:  gcc
BuildRequires:  cmake

%description
cJSON aims to be the dumbest possible parser that you can get your job
done with. It's a single file of C, and a single header file.
 
%package devel
Summary:        Development files for cJSON
Requires:       %{name}%{?_isa} = %{version}-%{release}
  
%description devel
The cjson-devel package contains libraries and header files for
developing applications that use cJSON.
  
%prep
%autosetup -n cJSON-%{version} -p1

%build
%cmake
%cmake_build

%install
%cmake_install

%files
%license LICENSE
%doc README.md
%{_libdir}/libcjson*.so.*
 
%files devel
%doc CHANGELOG.md CONTRIBUTORS.md
%{_libdir}/libcjson.so
%{_libdir}/pkgconfig/libcjson.pc
%{_libdir}/cmake/cJSON
%{_includedir}/cjson/

%changelog
* Fri Nov 22 2024 xiejing <xiejing@kylinos.cn> - 1.7.18-3
- Fix to check for null in cJSON_DetachItemViaPointer

* Mon Nov 04 2024 Funda Wang <fundawang@yeah.net> - 1.7.18-2
- adopt to new cmake macro

* Fri Aug 16 2024 Funda Wang <fundawang@yeah.net> - 1.7.18-1
- update to 1.7.18

* Wed May 29 2024 Zhao Mengmeng <zhaomengmeng@kylinos.cn> - 1.7.15-9
- Set free'd pointers to NULL to avoid double free

* Wed May 22 2024 xiejing <xiejing@kylinos.cn> - 1.7.15-8
- Fix heap buffer overflow

* Fri May 10 2024 wuzhaomin <wuzhaomin@kylinos.cn> - 1.7.15-7
- Fix print int without decimal places

* Wed May 8 2024 wuzhaomin <wuzhaomin@kylinos.cn> - 1.7.15-6
- add allocate check for replace_item_in_object

* Tue May 07 2024 xiaozai <xiaozai@kylinos.cn> - 1.7.15-5
- Fix a null pointer crash in cJSON_ReplaceItemViaPointer

* Fri Apr 26 2024 lvfei <lvfei@kylinos.cn> - 1.7.15-4
- fix CVE-2024-31755

* Tue Mar 05 2024 xiejing <xiejing@kylinos.cn> - 1.7.15-3
- fix potential memory leak in merge_patch()

* Sun Dec 24 2023 liningjie <liningjie@xfusion.com> - 1.7.15-2
- Fix CVE-2023-50471 CVE-2023-50472

* Thu Sep 23 2021 jiangxinyu <jiangxinyu@kylinos.cn> - 1.7.15-1
- Package Init
